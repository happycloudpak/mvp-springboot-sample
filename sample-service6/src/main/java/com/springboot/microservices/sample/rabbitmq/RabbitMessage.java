package com.springboot.microservices.sample.rabbitmq;

import lombok.Data;

@Data
public class RabbitMessage<T> {
	private T t;
}
